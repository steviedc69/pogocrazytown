import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import { Pokemon } from '../model/message';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
    
    private baseSpriteUrl: string = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';
  
    getPokemonSprite(pokemon:Pokemon) {
      console.log(pokemon);
      return `${this.baseSpriteUrl}${pokemon}.png`;
    }
  }

