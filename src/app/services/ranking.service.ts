import { Injectable, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from '../model/user';
import { map } from 'rxjs/operators';



const USERS: User[] = [];

@Injectable({
  providedIn: 'root'
})
export class RankingService {
// TODO firestore logic
  users : Observable<User[]>;
  private userList: User[]= [];
  
  constructor() {
    this.getUsers();
   }

  getUsers() :Observable<User[]>{
    if(!this.users){
      this.users = of(this.userList);
    }
    return this.users;
  }

  addUser(user:User){
    // if(!this.users){
    //   this.users = of([]);
    // }
    this.userList.push(user);
    this.users = of(this.userList);
  }
}
