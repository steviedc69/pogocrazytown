import { Injectable } from '@angular/core';
import { User } from '../model/user';
import levensthein from 'fast-levenshtein';
import { Observable, of } from 'rxjs';
import { Message, Type, MessagePokemon } from '../model/message';
import { type } from 'os';


@Injectable({
  providedIn: 'root'
})
export class AnalyzeService {

  level;
  name;
  battleWon;
  distanceWalk;
  pokeCaught;
  messages: Observable<Message[]>;
  messagesList: Message[] = [];

  constructor() { }

  getMessages():Observable<Message[]>{
    this.messages = of(this.messagesList);
    return this.messages;
  }

  analyseData(data, name) {
    let isU = false;
    let words = [];
    if (data) {
      if (data.lines.length > 0) {
        data.lines.forEach(line => {
          line.words.forEach(word => {
            if (!isU) {
              if (this.isUser(word.text.trim(), name)) {
                isU = true;
              }
            }
            else {
              words.push(word.text.trim());
            }
          });
        });
      }
      if (words.length > 0) {
        this.addMessage(Type.SUCCESS,"The image has been analysed, values are being calculated!");
        return this.createUser(words, name);
      }
    }
    this.addMessage(Type.ERROR, "The image could not be analysed!");
    return null;
  }

  private getDistance(distance: string) {
    let distanceR = +distance.replace(',', '');
    console.log("replace", distanceR)
    return +distanceR;
  }

  private isUser(word, name) {
    let lowerWord = word.toLowerCase();
    let lowerName = name.toLowerCase();
    if (lowerWord === lowerName ) {
      this.addMessage(Type.SUCCESS, "The screenname matches the image!");
      return true;
    }
    else {
      let length = lowerName.length;
      let equalN = levensthein.get(lowerWord, lowerName);
      let percent = equalN / length * 100;
      console.log("isUser", lowerName + " equal " + lowerWord + " " + percent)
      if (percent <= 40) {
        this.addMessage(Type.WARNING,"The screenname is not fully correct!");
        return true;
      }
      this.addMessage(Type.ERROR,"The screenname did not match with the image!",true);
      return false;
    }
  }

  errorList:Message[];
  private addMessage(type: Type, message,isLoop?:boolean) {
    let successCount = 0;
    if(type == Type.ERROR && isLoop){
      if(!this.errorList){
        this.errorList = [];
      }
      this.errorList.push(new MessagePokemon(type,message));
    }
    else if(type == Type.ERROR && !isLoop){
      this.messagesList.push(this.errorList[0]);
      this.messagesList.push(new MessagePokemon(type,message))
    }
    else {
        this.messagesList.push(new MessagePokemon(type,message));
        this.messagesList = this.messagesList.reverse();
        successCount++;
    }
    
    // this.messages = of(this.messagesList);
    // console.log(this.messagesList);
  }

  private createUser(words, name): User {
    this.name = name;
    let levelIndex = words.indexOf("Level");
    let battleIndex = words.indexOf("Won");
    let pokeCaughtIndex = words.indexOf("Caught");
    let distanceIndex = words.indexOf("km");
    if (levelIndex > -1) {
      this.level = words[levelIndex + 1];
    }
    else{
      this.addMessage(Type.WARNING,"The level was not found on the image!");
    }
    if (battleIndex > -1) {
      this.battleWon = +words[battleIndex + 1];
    }
    if (pokeCaughtIndex > -1) {
      this.pokeCaught = +words[pokeCaughtIndex + 1];
    }
    if (distanceIndex > -1) {
      this.distanceWalk = this.getDistance(words[distanceIndex - 1]);
    }

    if (this.isCorrect()) {
      let total = this.pokeCaught + this.battleWon + this.distanceWalk;
      return {
        name: this.name,
        level: this.level,
        pokeCaught: this.pokeCaught,
        battleWon: this.battleWon,
        distanceWalk: this.distanceWalk,
        total: total
      }
    }
    this.addMessage(Type.ERROR,"Something went wrong analysing the values!");
    return null;
  }


  private isCorrect(): boolean {
    if (this.battleWon && this.pokeCaught && this.distanceWalk) {
      return true;
    }
    return false;
  }
}
