import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartWindowComponent } from './main/start-window/start-window.component';
import { UserComponent } from './main/user/user.component';


const routes: Routes = [
  { path: "", component: StartWindowComponent },
  { path: "upload", component: UserComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
