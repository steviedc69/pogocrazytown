import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoadingComponent } from 'src/app/ui-models/loading/loading.component';
import { Message } from 'src/app/model/message';
import { AnalyzeService } from 'src/app/services/analyze.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  loading;
  user:User;
  imgURL: any;
  messages:Message[];
  userPopoverMessage:Message;

  constructor(private modal:NgbModal, private analyseService:AnalyzeService) { }

  ngOnInit(): void {
    this.getMessages();
  }

  submitImage() {

  }

  getMessages(){
    this.messages = [];
    this.analyseService.getMessages().subscribe(
      list => this.messages = list
    )
  }

  reset(){
    this.user = null;
    this.imgURL = null;
  }

  setUser(user) {
    this.close();
    if(user){
      this.user = user;
    }
  }

  setImgUrl(image){
    this.imgURL = image;
  }

  open(){
    this.modal.open(LoadingComponent, {windowClass: "bg-half-transparent", backdrop:"static",keyboard:false});
  }

  close(){
    this.modal.dismissAll();
  }

}
