import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user';
import { UserComponent } from '../user.component';
import { createWorker } from 'tesseract.js';
import { AnalyzeService } from 'src/app/services/analyze.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoadingComponent } from 'src/app/ui-models/loading/loading.component';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  user: User;
  username: string;
  userLevel: number;
  errorMessage;
  imagePath;
  imgUrl;
  popoverMessageName;
  isInputError:boolean;

  constructor(private analyseService: AnalyzeService, 
    private userComponent: UserComponent) { }

  ngOnInit(): void {
    this.isInputError = false;
    this.popoverMessageName = "Set your level. Must be equal to your level on Pogo!";
  }

  setUserName(name) {
    if (name.length > 3) {
      this.username = name;
    }
    else{

    }
  }
  setUserLevel(level) {
    this.userLevel = level;
  }

  preview(files) {
    this.open();
    if (this.username && this.userLevel) {
      if (files.length === 0) {
        this.close("files empty");
        return;
      }
      var mimeType = files[0].type;
      if (mimeType.match(/image\/*/) == null) {
        this.errorMessage = "Only images are supported.";
        this.close(this.errorMessage);
        return;
      }

      var reader = new FileReader();
      this.imagePath = files;
      reader.readAsDataURL(files[0]);
      reader.onload = (_event) => {
        this.imgUrl = reader.result;
        this.userComponent.setImgUrl(this.imgUrl);
        this.test();
      }
    }
    else{
      this.errorMessage = "Username is verplicht!";
      this.close(this.errorMessage);
      return;
    }

  }

  async test() {
    if (this.imgUrl) {
      const worker = createWorker();
      await worker.load();
      await worker.loadLanguage('eng');
      await worker.initialize('eng');
      await worker.recognize(this.imgUrl).then(
        data => {
          console.log(data);
          this.user = this.analyseService.analyseData(data.data, this.username);
          if(!this.user){
            this.isInputError = true;
            this.popoverMessageName = "Screenname or level did not match!";
            this.close(this.errorMessage);
          }
          else{
            this.userComponent.setUser(this.user);
          }          
        },
        reject => {
          console.log("test rejected", reject);
          this.errorMessage = "oeps";
          this.close(this.errorMessage);
        }
      );
      await worker.terminate();
    }
  }

  open(){
    console.log("create uset open");
    this.userComponent.open();
  }

  close(message){
    console.log("create uset close",message);
    this.userComponent.close();
  }



}
