import { Component, OnInit, Input } from '@angular/core';
import { AnalyzeService } from 'src/app/services/analyze.service';
import { Observable } from 'rxjs';
import { Message } from 'src/app/model/message';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  @Input()
  messages:Message[];

  constructor(private analyseService:AnalyzeService) { }

  ngOnInit(): void {
  }

  // TODO 
}
