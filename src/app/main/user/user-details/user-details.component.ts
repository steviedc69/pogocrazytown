import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/model/user';
import {NgbPopoverConfig} from '@ng-bootstrap/ng-bootstrap';
import { UserComponent } from '../user.component';
import { RankingService } from 'src/app/services/ranking.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css'],
  providers: [NgbPopoverConfig]
})
export class UserDetailsComponent implements OnInit {

  @Input()  
  user:User;


  constructor(config:NgbPopoverConfig, private rankingService:RankingService,private router:Router, private userComponent:UserComponent) {
    config.container="body";
    config.triggers="hover";
   }

  ngOnInit(): void {
  }

  setPopoverText(value){
    if(!value || value===''){
      return "Value was not recognized on the image! Please, correct the value.";
    }
    return "Value matches the image.";
  }

  addUser(){
    console.log("user",this.user);
    this.rankingService.addUser(this.user);
    this.router.navigate([""]);
  }

  reset(){
    this.userComponent.reset();
  }

}
