import { Component, OnInit, HostListener } from '@angular/core';
import { RankingService } from 'src/app/services/ranking.service';
import { User } from 'src/app/model/user';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css']
})
export class RankingComponent implements OnInit {

  users;
  innerWidth: any;


  constructor(private rankingService: RankingService) { }

  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    this.users = this.rankingService.users;

  }

  @HostListener('window:resize',['$event'])
  onResize(event){
    this.innerWidth = window.innerWidth;
    console.log('Resize',this.innerWidth);
  }

  findUsers() {
    this.users = [];
    this.rankingService.getUsers().subscribe(
      items => {
        return this.users = items.sort((a,b)=> b.total - a.total);
      }
    )
  }
}
