import { Component, OnInit } from '@angular/core';
import { createWorker} from 'tesseract.js'
import { AnalyzeService } from './services/analyze.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  

  constructor(){}

  ngOnInit(): void {
  }

}
