
export interface Message{
    type:Type,
    message:String,
    inputFieldName?:String,
    pokemon?:Pokemon
    prefix:String
} 

export class MessagePokemon implements Message{
    type: Type;
    message: String;
    inputFieldName?: String;
    pokemon: Pokemon;
    prefix:String;
 
    constructor(type, message){
        this.type = type;
        this.message = message;
        this.pokemon = this.setPokemon(this.type);
        this.prefix = this.setPrefix(this.type);
    }

    setPrefix(type):String{
        switch(type){
            case Type.ERROR : return "Wild CHARMANDER appeared!";
            case Type.WARNING : return "Wild PIKACHU appeared!";
            case Type.SUCCESS : return "Wild BULBASAUR appeared!";
        }
    }

    setPokemon(type): Pokemon{
        switch(type){
            case Type.ERROR : return Pokemon.CHARMANDER;
            case Type.WARNING : return Pokemon.PIKACHU;
            case Type.SUCCESS : return Pokemon.BULBASAUR;
        }
    }
}

export enum Pokemon {
    CHARIZARD= 6,
    PIKACHU= 25,
    BULBASAUR= 1,
    CHARMANDER=4
}

export enum Type{
    ERROR= "danger",
    SUCCESS = "success",
    WARNING = "warning"
}