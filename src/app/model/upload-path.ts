import { User } from './user';

export interface UploadPath {
    pathId:number,
    pathTitle:string,
    pathUser:User,
    pathError:string,
}
