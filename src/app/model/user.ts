export interface User {
    id? : string, 
    name: string,
    level: number,
    battleWon?: number,
    distanceWalk?: number,
    pokeCaught?:number,
    lastEdit?:Date,
    total?:number
    imgUrl?:any
}
