import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FontAwesomeModule, FaIconLibrary} from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './main/navbar/navbar.component';
import { StartWindowComponent } from './main/start-window/start-window.component';
import { RankingComponent } from './main/ranking/ranking.component';
import { UserComponent } from './main/user/user.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { LoadingComponent } from './ui-models/loading/loading.component';
import { ImageCardComponent } from './main/user/image-card/image-card.component';
import { UserDetailsComponent } from './main/user/user-details/user-details.component';
import { CreateUserComponent } from './main/user/create-user/create-user.component';
import { MessagesComponent } from './main/user/messages/messages.component';
import { AlertComponent } from './ui-models/alert/alert.component';
import { TitleComponent } from './main/title/title.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    StartWindowComponent,
    RankingComponent,
    UserComponent,
    LoadingComponent,
    ImageCardComponent,
    UserDetailsComponent,
    CreateUserComponent,
    MessagesComponent,
    AlertComponent,
    TitleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(library:FaIconLibrary){
    library.addIconPacks(fas);
  }
 }
