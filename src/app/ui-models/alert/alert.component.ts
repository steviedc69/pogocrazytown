import { Component, OnInit, Input } from '@angular/core';
import { Message } from 'src/app/model/message';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  @Input()
  message: Message;
  pokeImage: String;

  staticAlertClosed;

  constructor(private pokeService: PokemonService) { }

  ngOnInit(): void {
    this.pokeImage = this.pokeService.getPokemonSprite(this.message.pokemon);
    console.log(this.pokeImage);
    // setTimeout(() => this.staticAlertClosed = true, 10000);

  }

}
